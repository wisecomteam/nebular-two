
# 루트 
- .travis.yml : 커스텀 빌드(아직 사용하지 않음) 
- .stylelintrc.json : lint 자바스크립트 검사기(아직 사용하지 않음) 
// .angular-cli.json : paths 추가, 임포트를 편하게 하기위해서 추가하는것 같음

# src 폴드 ( 2개 확인 )
- tsconfig.app.json
- typings.d.ts : 변수추가 된부분 화인 



# UI 추가 (ngx-sample-layout) 2017-10-21
- 패키지 추가
    "@nebular/auth": "2.0.0-rc.2",
    "@nebular/theme": "2.0.0-rc.2",
    "bootstrap": "4.0.0-alpha.6",
    "@ng-bootstrap/ng-bootstrap": "1.0.0-alpha.26",
    "tinymce": "4.5.7"          --> 에디터

- @core 디렉토리 추가 : 기능확인 필요
- @theme 디렉토리 추가 : 기능확인 필요
- ./pages/ui-features 디렉토리 추가 : 기능확인 필요

- .angular-cli.json 배포 정보 샛팅
 챠트 스트립트 아직 포함하지 않음 ???
  protractor.conf.js e2e에서 포함

# UI 추가 부분 확인 후 분석문서 작성
