import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';

@Component({
    selector: 'ngx-pages',
    template: `
      <ngx-sample-layout>
      </ngx-sample-layout>
      `,
  })
  export class PagesComponent {

    menu = MENU_ITEMS;
  }
  