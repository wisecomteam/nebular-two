/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

// 이모듈이 왜 자동으로 올라가는지 확인 필요 ???
declare var tinymce: any;

declare var echarts: any;
